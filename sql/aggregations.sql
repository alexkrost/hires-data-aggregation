create table panther.hires_aggregated_ats_jb (
    "date" DATE,
    ats_req_id VARCHAR,
    job_board VARCHAR(255),
    num_hires INT,
    PRIMARY KEY ("date", ats_req_id, job_board)
)