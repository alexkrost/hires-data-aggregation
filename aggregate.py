import asyncio
import argparse

from etl_utils.config_parser import CP
from etl_utils.database_interface import (
    init_db_interface,
    run_query,
)


async def agg_ats_jb(hires_table):
    query = f"""insert into panther.hires_aggregated_ats_jb as agg
                (date, ats_req_id, job_board, num_hires)
                select date, ats_req_id, job_board, count(*) num_hires
                from {hires_table}
                group by date, ats_req_id, job_board
                on conflict (date, ats_req_id, job_board)
                do update set
                num_hires = EXCLUDED.num_hires"""
    await run_query(query, pgres=True)


async def main():
    parser = argparse.ArgumentParser(prog="PROG")
    parser.add_argument("-c", "--config", required=True, help="Configuration file")
    parser.add_argument(
        "-t",
        "--table-name",
        dest="hires_table_name",
        help="De-duplicated table name in the format 'schema.table'",
    )
    args = parser.parse_args()

    CP.init(args)
    await init_db_interface()
    await asyncio.gather(*[agg_ats_jb(args.hires_table_name)])


if __name__ == "__main__":
    asyncio.get_event_loop().run_until_complete(main())