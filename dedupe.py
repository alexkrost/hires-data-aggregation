import uuid
import asyncio
import argparse

from etl_utils.config_parser import CP
from etl_utils.database_interface import (
    init_db_interface,
    run_query,
)


async def get_last_proc_ts(pg_table_name):
    query = f"""select coalesce(max(proc_ts), '2020-01-01 00:00:00.000000'::TIMESTAMP) from {pg_table_name}"""
    max_ts = await run_query(query, pgres=True, with_result=True)
    return max_ts[0][0]


async def move_data_from_rs_to_pg(rs_table, pg_table, min_proc_time):
    connection_id = str(uuid.uuid4()).replace("-", "")
    connection_query = f"""
    SELECT
       dblink_connect(concat('redshift_conn_v2_', '{connection_id}'), 'host=' || host || ' port=' || port || ' dbname=' || dbname || ' user=' || username || ' password=' || pass)
    FROM
       dblink_creds
    WHERE
       profile_id = 1;
    """
    await run_query(connection_query, pgres=True)

    query = f"""
        insert into {pg_table} as dedup
            (proc_ts, "date", customer_id, candidate_id, ats_req_id, domain_user_id, perengo_domain_user_id, client_domain_user_id,
             supercampaign_id, short_id, tenant_id, ats_file_configuration_id, ats_file_id, application_date, application_id,
             ats_source_code, ats_source_code_details, hire_date, hire_status, gender, hires, load_date, job_board,
             job_campaign_id, market_id, "category", zipcode, population, landing_page_url, job_title, segment_id,
             segment_filter_label, segment_filter_priority, flight_id, flight_filter_label, flight_filter_priority,
             bucket, "key")
        SELECT *
             from dblink(concat('redshift_conn_v2_', '{connection_id}'),
                 CONCAT('
                    -- To avoid inserting multiple rows that cause conflicts (which would cause the insertion to fail)
                    -- get the last row for the pkey columns
                    with ranked as (
                        select
                            proc_ts, "date", customer_id, candidate_id, req_id, domain_user_id, perengo_domain_user_id, client_domain_user_id,
                            supercampaign_id, short_id, tenant_id, ats_file_configuration_id, ats_file_id, application_date, application_id,
                            ats_source_code, ats_source_code_details, hire_date, hire_status, gender, hires, load_date, job_board,
                            job_campaign_id, market_id, "category", zipcode, population, landing_page_url, job_title, segment_id,
                            segment_filter_label, segment_filter_priority, flight_id, flight_filter_label, flight_filter_priority,
                            bucket, "key", rank() over (partition by "date", customer_id, candidate_id, req_id, perengo_domain_user_id,
                                                        client_domain_user_id, short_id, "key" order by proc_ts desc, hire_date asc) as _r
                        from {rs_table}
                        where proc_ts >= ''{min_proc_time}''
                        and "key" not like ''%Simulated%''
                    )
                    select
                        proc_ts, "date", customer_id, candidate_id, req_id, domain_user_id, perengo_domain_user_id, client_domain_user_id,
                        supercampaign_id, short_id, tenant_id, ats_file_configuration_id, ats_file_id, application_date, application_id,
                        ats_source_code, ats_source_code_details, hire_date, hire_status, gender, hires, load_date, job_board,
                        job_campaign_id, market_id, "category", zipcode, population, landing_page_url, job_title, segment_id,
                        segment_filter_label, segment_filter_priority, flight_id, flight_filter_label, flight_filter_priority,
                        bucket, "key"
                    from ranked
                    where _r = 1
                ')) as t (
                    proc_ts TIMESTAMP,
                    "date" DATE,
                    customer_id BIGINT,
                    candidate_id TEXT,
                    ats_req_id TEXT,
                    domain_user_id TEXT,
                    perengo_domain_user_id TEXT,
                    client_domain_user_id TEXT,
                    supercampaign_id BIGINT,
                    short_id VARCHAR,
                    tenant_id VARCHAR,
                    ats_file_configuration_id VARCHAR,
                    ats_file_id VARCHAR,
                    application_date TIMESTAMP,
                    application_id VARCHAR,
                    ats_source_code VARCHAR,
                    ats_source_code_details VARCHAR,
                    hire_date TIMESTAMP,
                    hire_status VARCHAR,
                    gender VARCHAR,
                    hires BIGINT,
                    load_date TIMESTAMP,
                    job_board VARCHAR,
                    job_campaign_id BIGINT,
                    market_id BIGINT,
                    category VARCHAR,
                    zipcode VARCHAR,
                    population BIGINT,
                    landing_page_url VARCHAR(1024),
                    job_title VARCHAR(255),
                    segment_id INT,
                    segment_filter_label VARCHAR(4096),
                    segment_filter_priority INT,
                    flight_id INT,
                    flight_filter_label VARCHAR(4096),
                    flight_filter_priority INT,
                    bucket TEXT,
                    "key" TEXT
                )
        on conflict ("date", customer_id, candidate_id, ats_req_id, perengo_domain_user_id, client_domain_user_id, short_id, "key") do update set
            (proc_ts, "date", customer_id, candidate_id, ats_req_id, domain_user_id, perengo_domain_user_id, client_domain_user_id,
             supercampaign_id, short_id, tenant_id, ats_file_configuration_id, ats_file_id, application_date, application_id,
             ats_source_code, ats_source_code_details, hire_date, hire_status, gender, hires, load_date, job_board,
             job_campaign_id, market_id, "category", zipcode, population, landing_page_url, job_title, segment_id,
             segment_filter_label, segment_filter_priority, flight_id, flight_filter_label, flight_filter_priority,
             bucket, "key")
        =   (EXCLUDED.proc_ts, EXCLUDED."date", EXCLUDED.customer_id, EXCLUDED.candidate_id, EXCLUDED.ats_req_id,
             EXCLUDED.domain_user_id, EXCLUDED.perengo_domain_user_id, EXCLUDED.client_domain_user_id,
             EXCLUDED.supercampaign_id, EXCLUDED.short_id, EXCLUDED.tenant_id, EXCLUDED.ats_file_configuration_id,
             EXCLUDED.ats_file_id, EXCLUDED.application_date, EXCLUDED.application_id, EXCLUDED.ats_source_code,
             EXCLUDED.ats_source_code_details, EXCLUDED.hire_date, EXCLUDED.hire_status, EXCLUDED.gender, EXCLUDED.hires,
             EXCLUDED.load_date, EXCLUDED.job_board, EXCLUDED.job_campaign_id, EXCLUDED.market_id, EXCLUDED."category",
             EXCLUDED.zipcode, EXCLUDED.population, EXCLUDED.landing_page_url, EXCLUDED.job_title, EXCLUDED.segment_id,
             EXCLUDED.segment_filter_label, EXCLUDED.segment_filter_priority, EXCLUDED.flight_id, EXCLUDED.flight_filter_label,
             EXCLUDED.flight_filter_priority, EXCLUDED.bucket, EXCLUDED."key")
        where dedup.proc_ts < EXCLUDED.proc_ts;
        """
    await run_query(query, pgres=True)


async def main():
    parser = argparse.ArgumentParser(prog="PROG")
    parser.add_argument("-c", "--config", required=True, help="Configuration file")
    parser.add_argument(
        "-pgt",
        "--pg-table-name",
        dest="pg_table_name",
        help="De-duplicated table name in the format 'schema.table'",
    )
    parser.add_argument(
        "-rst",
        "--rs-table-name",
        dest="rs_table_name",
        help="Non-de-duplicated table name in the format 'schema.table'",
    )

    args = parser.parse_args()

    CP.init(args)
    await init_db_interface()

    max_ts = await get_last_proc_ts(args.pg_table_name)
    await move_data_from_rs_to_pg(args.rs_table_name, args.pg_table_name, max_ts)


if __name__ == "__main__":
    asyncio.get_event_loop().run_until_complete(main())
